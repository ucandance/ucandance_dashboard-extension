<?php namespace Finnito\UcandanceDashboardExtension;

// use Anomaly\Streams\Platform\Addon\Extension\Extension;
use Anomaly\DashboardModule\Widget\Contract\WidgetInterface;
use Anomaly\DashboardModule\Widget\Extension\WidgetExtension;
use Finnito\UcandanceDashboardExtension\Command\LoadDash;


class UcandanceDashboardExtension extends WidgetExtension
{

    /**
     * This extension provides...
     *
     * This should contain the dot namespace
     * of the addon this extension is for followed
     * by the purpose.variation of the extension.
     *
     * For example anomaly.module.store::gateway.stripe
     *
     * @var null|string
     */
        protected $provides = 'anomaly.module.dashboard::widget.ucandance_dash';

    /**
     * Load the widget data.
     *
     * @param WidgetInterface $widget
     */
    protected function load(WidgetInterface $widget)
    {
        $this->dispatch(new LoadDash($widget));
    }


}
