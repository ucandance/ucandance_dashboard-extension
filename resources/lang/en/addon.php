<?php

return [
    'title'       => 'UCanDance Dashboard',
    'name'        => 'UCanDance Dashboard Extension',
    'description' => 'A dashboard of various data to for the committee to see at a glance.'
];
